# Proof Complexity Zoo

Inspired by the [Complexity
Zoo](https://complexityzoo.net/Complexity_Zoo) and more precisely by
its automated counterpart the [Complexity
Zoology](https://www.math.ucdavis.edu/~greg/zoology/intro.html), the
proof complexity zoo is a community-editable database of proof
systems, formulas, and relations among them. It can be browsed at
https://proofcomplexityzoo.gitlab.io/zoo/.

Right now the project is just a proof of concept, and suggestions on
which data to store, which queries to support, and how to present the
information are particularly welcome. The database can be edited at
https://gitlab.com/proofcomplexityzoo/zoo/-/edit/master/zoo.yaml after
creating a user account and asking for permissions. Suggestions on how
to make editing more seamless are also welcome. Of course, patches are
even better!
