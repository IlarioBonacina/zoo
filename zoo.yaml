# Mandatory: longname
# Optional: notes
proofsystems:
  res:
    longname: "Resolution"
    subsystemof:
      - ac0frege
      - circres
      - reslinF2
      - extres
  treeres:
    longname: "Tree-like resolution"
    subsystemof:
      - regres
      - treereslinF2
  regres:
    longname: "Regular resolution"
    subsystemof: res
  ordres:
    longname: "Ordered resolution"
    subsystemof: regres
  poolres:
    longname: "Pool resolution"
    subsystemof: res
  circres:
    longname: "Circular resolution"
  extres:
    longname: "Extended resolution"
    subsystemof: extfrege

  cp:
    longname: "Cutting Planes"
    subsystemof: semanticcp
  treecp:
    longname: "Tree-like Cutting Planes"
    subsystemof: cp
  unarycp:
    longname: "Cutting Planes with Unary Coefficients"
    subsystemof: cp
  semanticcp:
    longname: "Semantic Cutting Planes"
  saturationcp:
    longname: "Cutting Planes with Saturation"
    subsystemof: semanticcp
  sp:
    longname: "Stabbing Planes"
    source: BFIKPPR18
  unarysp:
    longname: "Stabbing Planes with Unary Coefficients"
    subsystemof: sp

  pcF2:
    longname: "Polynomial Calculus over \\(\\mathbb{F}_2\\)"
    notes: "With twin variables \\(\\overline{x}=1-x\\)"
  nssF2:
    longname: "Nullstellensatz over \\(\\mathbb{F}_2\\)"
    subsystemof:
      - pcF2
    notes: "With twin variables \\(\\overline{x}=1-x\\)"
  reslinF2:
    longname: "ResLin over \\(\\mathbb{F}_2\\)"
    subsystemof: ac0fregemod2gates
  treereslinF2:
    longname: "Tree-like ResLin over \\(\\mathbb{F}_2\\)"
    subsystemof: reslinF2

  pcQ:
    longname: "Polynomial Calculus over \\(\\mathbb{Q}\\)"
    notes: "With twin variables \\(\\overline{x}=1-x\\)"
  nssQ:
    longname: "Nullstellensatz over \\(\\mathbb{Q}\\)"
    subsystemof:
      - pcQ
      - sa
    notes: "With twin variables \\(\\overline{x}=1-x\\)"
    
  pssc:
    longname: "Positivstellensatz Calculus"
  pss:
    longname: "Positivstellensatz"
    subsystemof: pssc
    notes: "Grigoriev's version, but with Boolean axioms"
  ls:
    longname: "Lovász--Schrijver"
  sos:
    longname: "Sum of Squares (Lasserre)"
    subsystemof: pss
  sa:
    longname: "Sherali--Adams"
    notes: "With twin variables \\(\\overline{x}=1-x\\)"

  extfrege:
    longname: "Extended Frege"
  frege:
    longname: "Frege"
    subsystemof: extfrege
  ac0frege:
    longname: "\\(\\mathrm{AC}^0\\)-Frege"
    subsystemof:
      - frege
      - tc0frege
      - ac0fregemod2axioms
      - ac0fregemod2gates
  tc0frege:
    longname: "\\(\\mathrm{TC}^0\\)-Frege"
  ac0fregemod2axioms:
    longname: "\\(\\mathrm{AC}^0\\)-Frege with mod 2 axioms"
  ac0fregemod2gates:
    longname: "\\(\\mathrm{AC}^0\\)-Frege with mod 2 gates"

  obddjoinweak:
    longname: "OBDD(join,weakening)"

# Mandatory: longname
# Optional: extends (another formula)
formulas:
  php:
    longname: "Pigeonhole principle"
  fphp:
    longname: "Pigeonhole principle with functionality axioms"
    extends: php
  ofphp:
    longname: "Pigeonhole principle with functionality and onto axioms"
    extends: fphp
  php2nn:
    longname: "Weak pigeonhole principle (2n pigeons, n holes)"
    extends: php
  phpn2n:
    longname: "Weak pigeonhole principle (\\(n^2\\) pigeons, n holes)"
    extends: php2nn
  ordering:
    longname: "Ordering principle"
    source: Krishnamurthy85
  tseitin:
    longname: "Tseitin"
  random:
    longname: "Random CNF"
  cliquecolouring:
    longname: "Clique-Colouring"
  cliquecolouringeq:
    longname: "Clique-Colouring encoded as equalities"
    source: FHL16
  cliquecolouring2mm:
    longname: "Weak Clique-Colouring (2m clique, m colours)"
    extends: cliquecolouring
  cliquecolouringperm:
    longname: "Clique-Colouring composed with a permutation"
  peb+ind:
    longname: "Pebbling \\(\\circ\\) IND"
  stone:
    longname: "Stone formula"
    source: AJPU07
  pearl:
    longname: "String of pearls"
    source: BEGJ00

# Optional: size (default: poly; may be {poly, quasipoly, exp})
# Optional: source
upperbounds:
  unarycp:
    php
  cp:
    tseitin:
      size: quasipoly
      source: DT20
  res:
    ordering:
      source: Stalmarck96
    peb+ind:
      source: BEGJ00
    pearl:
      source: BEGJ00
  poolres:
    stone:
      source: BK14
  treeres:
    pearl:
      size: quasipoly
      source: Johannsen01
  pcF2:
    tseitin
  reslinF2:
    tseitin
  sa:
    php
  semanticcp:
    cliquecolouringeq
  extfrege:
    php:
      source: CR79
  frege:
    php:
      source: Buss87
  ac0frege:
    php2nn:
      size: quasipoly
    cliquecolouring2mm:
      size: quasipoly
  sp:
    tseitin:
      size: quasipoly
      source: BFIKPPR18

  obddjoinweak:
    tseitin:
      size: poly
      source: AKV04
    cliquecolouring:
      size: poly
      source: BIKS18

# Mandatory: size (may be {poly, quasipoly, exp})
# Optional: source
lowerbounds:
  res:
    ofphp:
      size: exp
      source: Haken85
    tseitin:
      size: exp
      source: Urquhart87
    random:
      size: exp
      source: CS88
    cliquecolouring:
      size: exp
      source: Krajicek97
  treeres:
    ordering:
      size: exp
  regres:
    stone:
      size: exp
      source: AJPU07
  ordres:
    pearl:
      size: exp
      source: BEGJ00
  ac0frege:
    ofphp:
      size: exp
      source:
        - PBI93
        - KPW95
    tseitin:
      size: exp
      source: BenSasson02
  pcF2:
    php:
      size: exp
  pcQ:
    php:
      size: exp
  cp:
    cliquecolouring:
      size: exp
      source: Pudlak97
    cliquecolouringeq:
      size: exp
      source: FHL16
    cliquecolouring2mm:
      size: exp
      source: Pudlak97
  semanticcp:
    cliquecolouring:
      size: exp
      source: FHL16
  treecp:
    peb+ind:
      size: exp
      source: BEGJ00
  treereslinF2:
    php:
      size: exp
      source: IS14

  obddjoinweak:
    cliquecolouringperm:
      size: exp
      source: Krajicek08

# First level: simulator
# Second level: simulated
# Optional: overhead (default: poly; may be {none, poly, quasipoly})
# Optional: source
simulations:
  unarycp:
    res:
      overhead: poly
  treecp:
    treeres:
      overhead: poly
  cp:
    unarysp:
      overhead: quasipoly
      source: FGIPRTW21
  sp:
    cp:
      overhead: poly
  pcF2:
    res:
      overhead: poly
  pcQ:
    res:
      overhead: poly
  sa:
    circres:
      overhead: poly
      source: AL19
  circres:
    sa:
      overhead: poly
      source: AL19
  saturationcp:
    res:
      overhead: poly
  res:
    saturationcp:
      overhead: poly
  extres:
    extfrege:
      overhead: poly
  frege:
    cp:
      overhead: poly
      source: Goerdt92
  ac0fregemod2axioms:
    nssF2:
      overhead: poly
      source: IS06
  ac0fregemod2gates:
    ac0fregemod2axioms:
      overhead: poly
  sos:
    sa:
      overhead: poly
    pcQ:
      overhead: poly
      source: Berkholz18
  pss:
    pssc:
      overhead: poly
      source: Berkholz18
  obddjoinweak:
    unarycp:
      source: AKV04

# Use journal versions if they exist
# Optional: dblp key
papers:
  CR79:
    dblp: journals/jsyml/CookR79
  Haken85:
    dblp: journals/tcs/Haken85
  Krishnamurthy85:
    dblp: journals/acta/Krishnamurthy85
  Buss87:
    dblp: journals/jsyml/Buss87
  Urquhart87:
    dblp: journals/jacm/Urquhart87
  CS88:
    dblp: journals/jacm/ChvatalS88
  Goerdt92:
    url: https://doi.org/10.1007/3-540-54487-9_59
  PBI93:
    dblp: journals/cc/PitassiBI93
  KPW95:
    url: https://doi.org/10.1002/rsa.3240070103
  Stalmarck96:
    dblp: journals/acta/Stalmarck96
  Krajicek97:
    dblp: journals/jsyml/Krajicek97
  Pudlak97:
    dblp: journals/jsyml/Pudlak97
  BEGJ00:
    dblp: journals/siamcomp/BonetEGJ00
  Johannsen01:
    url: https://www.tcs.ifi.lmu.de/mitarbeiter/jan-johannsen/publikationen-1/unpubl/string.pdf
  BenSasson02:
    dblp: journals/cc/Ben-Sasson02
  AKV04:
    dblp: conf/cp/AtseriasKV04
  IS06:
    dblp: journals/tocl/ImpagliazzoS06
  AJPU07:
    dblp: journals/toc/AlekhnovichJPU07
  Krajicek08:
    dblp: journals/jsyml/Krajicek08
  BK14:
    dblp: journals/corr/BussK14
  IS14:
    dblp: conf/mfcs/ItsyksonS14
  FHL16:
    dblp: conf/stacs/FilmusHL16
  Berkholz18:
    dblp: conf/stacs/Berkholz18
  BIKS18:
    dblp: conf/coco/BussIKS18
  BFIKPPR18:
    dblp: conf/innovations/BeameFIKPPR18
  AL19:
    dblp: conf/sat/AtseriasL19
  DT20:
    dblp: conf/coco/DadushT20
  FGIPRTW21:
    dblp: journals/eccc/FlemingGIPRTW21
